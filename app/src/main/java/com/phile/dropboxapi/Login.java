package com.phile.dropboxapi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.dropbox.core.android.Auth;

public class Login extends AppCompatActivity {

    private final String APP_KEY = "fekak8woziyf7dv";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if (Preferences.getInstance(this).getToken() != "")
            logged();
        else
            Auth.startOAuth2Authentication(getApplicationContext(), APP_KEY);
    }
    @Override
    protected void onResume() {
        super.onResume();
        getAccessToken();
    }

    public void getAccessToken() {
        String accessToken = Auth.getOAuth2Token();
        if (accessToken != null) {
            Preferences.getInstance(this).setToken(accessToken);
            logged();
        }
    }

    private void logged() {
        Intent intent = new Intent(Login.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
