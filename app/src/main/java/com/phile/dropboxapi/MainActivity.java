package com.phile.dropboxapi;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.ListFolderResult;
import com.dropbox.core.v2.files.Metadata;
import com.dropbox.core.v2.files.WriteMode;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final int PICKFILE_RESULT_CODE = 1;
    FloatingActionButton fabUpload;
    ListView lvFiles;
    TextView tvRuta;

    String token;
    String ruta = "/";
    DbxClientV2 client;
    List<DropboxItem> files;
    CustomAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lvFiles = (ListView) findViewById(R.id.lvFiles);
        fabUpload = (FloatingActionButton) findViewById(R.id.fabUpload);
        fabUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent fileintent = new Intent(Intent.ACTION_GET_CONTENT);
                fileintent.setType("gagt/sdf");
                try {
                    startActivityForResult(fileintent, PICKFILE_RESULT_CODE);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(MainActivity.this, "No tienes un explorador de archivos", Toast.LENGTH_SHORT).show();
                }
            }
        });
        tvRuta = (TextView) findViewById(R.id.tvRuta);

        files = new ArrayList<DropboxItem>();
        adapter = new CustomAdapter(this, files);
        lvFiles.setAdapter(adapter);

        startingAPI();

        lvFiles.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> adapterView, View view, final int position, long l) {
                if (adapter.getItem(position).getType() == DropboxItem.FOLDER_TYPE) {
                    getFiles(adapter.getItem(position).getPath());
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle(adapter.getItem(position).getFilename());
                    builder.setMessage("¿Deseas descargar el fichero?");
                    builder.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            download(adapter.getItem(position).getFilename(),adapter.getItem(position).getPath());
                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            });
                    builder.show();
                }
            }
        });
    }

    private void startingAPI() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                token = Preferences.getInstance(MainActivity.this).getToken();
                DbxRequestConfig config = new DbxRequestConfig("dropbox/java-tutorial", "en_US");
                client = new DbxClientV2(config, token);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getFiles("");
                    }
                });
            }
        }).start();
    }

    private void download(final String name, final String path) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), name);
                BufferedOutputStream bf = null;

                try {

                    bf = new BufferedOutputStream(new FileOutputStream(file));
                    client.files().download(path).download(bf);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.this, "Archivo descargado correctamente", Toast.LENGTH_SHORT).show();
                        }
                    });

                } catch (final Exception e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.this, "Ha ocurrido un error al descargar: "+ e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });

                } finally {

                    if (bf != null) {

                        try {
                            bf.close();
                        } catch (IOException e) {
                            //
                        }
                    }
                }
            }
        }).start();

    }

    private void upload(final String path) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    File file = new File(path);
                    // Upload to Dropbox
                    InputStream inputStream = new FileInputStream(file);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.this, "Subiendo el archivo...", Toast.LENGTH_SHORT).show();
                        }
                    });
                    client.files().uploadBuilder("/" + file.getName())
                            .withMode(WriteMode.OVERWRITE)
                            .uploadAndFinish(inputStream);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.this, "Archivo subido correctamente", Toast.LENGTH_SHORT).show();
                        }
                    });
                    if(ruta == "/")
                        ruta = "";
                    getFiles(ruta);
                } catch (DbxException | IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.this, "Error: "+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        }).start();

    }

    public void getFiles(final String path) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                files.clear();
                ListFolderResult result = null;
                try {
                    result = client.files().listFolder(path);
                    while (true) {
                        for (Metadata metadata : result.getEntries()) {

                            boolean isFileMetadata = metadata instanceof FileMetadata;
                            files.add(new DropboxItem(metadata.getPathDisplay(), metadata.getName(), isFileMetadata ? DropboxItem.FILE_TYPE:DropboxItem.FOLDER_TYPE));
                        }

                        if (!result.getHasMore()) {
                            break;
                        }

                        try {
                            result = client.files().listFolderContinue(result.getCursor());
                        } catch (DbxException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (DbxException e) {
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        listItems();
                        ruta = (path.equals("") ? "/" : path);
                        tvRuta.setText("Ruta actual: " + ruta);
                    }
                });
            }
        }).start();


    }

    private void listItems() {
        adapter.setItems(files);
    }

    @Override
    public void onBackPressed() {
        if (ruta != "/") {
            String rutaAux = "";
            String[] rutaSplit = ruta.split("/");
            for (int i = 0; i < rutaSplit.length; i++) {
                if (!rutaSplit[i].equals(""))
                    if ((i + 1) != rutaSplit.length)
                        rutaAux += "/" + rutaSplit[i];
            }
            rutaAux = rutaAux.equals("/") ? "" : rutaAux;
            getFiles(rutaAux);
        } else
            super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = new MenuInflater(this);
        inflater.inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_actualizar:
                getFiles(ruta);
                break;
            case R.id.action_cerrarsesion:
                Preferences.getInstance(this).setToken("");
                Intent intent = new Intent(MainActivity.this, Login.class);
                startActivity(intent);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICKFILE_RESULT_CODE && resultCode == RESULT_OK) {
            upload(data.getData().getPath());
        }
    }
}
