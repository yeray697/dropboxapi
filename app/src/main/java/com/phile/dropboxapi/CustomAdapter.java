package com.phile.dropboxapi;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by yeray697 on 11/12/16.
 */

public class CustomAdapter extends ArrayAdapter<DropboxItem> {
    public CustomAdapter(Context context, List<DropboxItem> objects) {
        super(context, R.layout.lv_item, new ArrayList<DropboxItem>(objects));
    }
    public void setItems(List<DropboxItem> objects) {
        clear();
        addAll(objects);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        Holder holder;

        if (view == null) {
            LayoutInflater inflater = ((Activity)getContext()).getLayoutInflater();
            view = inflater.inflate(R.layout.lv_item,null);
            holder = new Holder();
            holder.tvName = (TextView) view.findViewById(R.id.tvName);
            holder.ivType = (ImageView) view.findViewById(R.id.ivType);
            view.setTag(holder);
        } else {
            holder = (Holder) view.getTag();
        }

        DropboxItem aux = getItem(position);
        if (aux.getType() == DropboxItem.FILE_TYPE)
            holder.ivType.setImageDrawable(ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.file, null));
        else
            holder.ivType.setImageDrawable(ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.folder, null));
        holder.tvName.setText(aux.getFilename());
        aux = null;
        return view;
    }
    class Holder{
        TextView tvName;
        ImageView ivType;
    }
}
