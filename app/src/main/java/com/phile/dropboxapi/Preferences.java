package com.phile.dropboxapi;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.dropbox.core.v2.DbxClientV2;

/**
 * Created by yeray697 on 10/12/16.
 */
public class Preferences {
    private SharedPreferences sharedPreferences;

    private static Preferences ourInstance;

    public static Preferences getInstance(Context context) {
        if (ourInstance == null)
            ourInstance = new Preferences(context);
        return ourInstance;
    }

    private Preferences(Context context) {

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }
    private final String TOKEN_KEY = "token";

    public String getToken(){
        return sharedPreferences.getString(TOKEN_KEY, "");
    }
    public void setToken(String token){
        sharedPreferences.edit().putString(TOKEN_KEY, token).apply();
    }
}
