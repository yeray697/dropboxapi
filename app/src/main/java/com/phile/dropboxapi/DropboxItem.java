package com.phile.dropboxapi;

/**
 * Created by yeray697 on 11/12/16.
 */

public class DropboxItem {
    public static final int FILE_TYPE = 0;
    public static final int FOLDER_TYPE = 1;
    private String path;
    private String filename;
    private int type;

    public DropboxItem(String path, String filename, int type) {
        this.path = path;
        this.filename = filename;
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public String getFilename() {
        return filename;
    }

    public String getPath() {
        return path;
    }
}
