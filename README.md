#### Para hacer que pidiese el email y la contraseña he puesto esto en el manifest:         
    <activity
        android:name="com.dropbox.core.android.AuthActivity"
        android:configChanges="orientation|keyboard"
        android:launchMode="singleTask">
        <intent-filter>
            <data android:scheme="db-APP_KEY" />
            <action android:name="android.intent.action.VIEW" />
            <category android:name="android.intent.category.BROWSABLE" />
            <category android:name="android.intent.category.DEFAULT" />
        </intent-filter>
    </activity>
#### y en la activity
    Auth.startOAuth2Authentication(getApplicationContext(), APP_KEY);
.
#### Para la obtención de las API Keys he tenido que registrar mi aplicación en Dropbox (https://www.dropbox.com/developers/apps/create).
.
#### Para usar la API hace falta declarar un objeto del tipo DbxClientV2. Con él, he mostrado los elementos con un método que se llama files().listFolder(ruta). Para subir los archivos con el método files().uploadBuilder(ruta).withMode(WriteMode.OVERWRITE).uploadAndFinish(InputStream) y para descargarlos con files().download(ruta).download(BufferedOutputStream)